from django.urls import path
from .views import AboutPageView, ContactPageView, HomePageView, GalleryFrontPageView
from .views import CityPageView, PublicationsPageView, ProjectsPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('publications/', PublicationsPageView.as_view(), name='publications'),
    path('projects/', ProjectsPageView.as_view(), name='projects'),
    path('contact/', ContactPageView.as_view(), name='contact'),
    path('gallery/', GalleryFrontPageView.as_view(), name='gallery'),
    path('gallery/<slug>', CityPageView.as_view(), name='city'),


]