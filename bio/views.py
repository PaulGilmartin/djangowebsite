from django.shortcuts import render
from django.views.generic import ListView, TemplateView
from collections import namedtuple
from django.urls import reverse
from .models import Paper, Project, Picture

Link = namedtuple('Link', ['link', 'display', 'link_args'], defaults=('',))  # supplies a default value to link_args


class HomePageView(TemplateView):
    template_name = 'home.html'

    # TODO: is it worth abstracting this context gathering method as an inheritable extendable one?
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['links'] = [Link(link='projects', display='Projects'),
                            Link(link='publications', display='Publications'),
                            Link(link='gallery', display='Gallery'),]
        context['central_image'] = 'necropolis.jpg'
        return context


class AboutPageView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['local_links'] = []
        context['web_links'] = []
        return context


class ContactPageView(TemplateView):
    template_name = 'contact.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['links'] = [Link(link='projects', display='Projects'),
                            Link(link='publications', display='Publications'),
                            Link(link='gallery', display='Gallery'),]
        return context


class GalleryFrontPageView(TemplateView):
    template_name = 'gallery/gallery_front.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # SELECT DISTINCT location FROM Picture
        locations = {d['location'] for d in Picture.objects.values('location').distinct()}
        context['links'] = [Link(link='city', link_args=location, display=location) for location in locations]
        context['central_image'] = ''
        return context


class PublicationsPageView(ListView):
    template_name = 'publications.html'
    model = Paper
    context_object_name = 'papers'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Publications'
        context['blurb'] = """My research interests lie primarily in the field of non-commutative algebra.
                             I'm particularly interested in Hopf algebras of finite GK-dimension, quantum groups,
                             quantum homogeneous spaces and Poisson algebraic groups. 
                             My published research papers in this field are listed below."""
        return context


class ProjectsPageView(ListView):
    template_name = 'projects.html'
    model = Project
    context_object_name = 'papers'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Projects'
        context['blurb'] = """This page details some of the programming projects I've worked on in my spare time.
                            More to come in the not too distant future."""
        return context


class CityPageView(ListView):
    template_name = 'gallery/city.html'
    model = Picture

    # e.g. when user requests paulgilmartin.co.uk/gallery/Glasgow,
    # CityPageView self.kwargs is populated with 'slug': Glasgow.
    # Then override get_queryset (which by default would return all
    # Picture objects) to return pictures with city Glasgow
    def get_queryset(self, *args, **kwargs):
        all_images_of_location = self.model.objects.filter(location=self.kwargs['slug'])
        portrait_images = all_images_of_location.filter(orientation='Portrait')
        landscape_images = all_images_of_location.filter(orientation='Landscape')
        # make this nicer - idea is to create a list here so that our final image display has
        # rows which have pictures all with the same orientation and at length most 3
        return [landscape_images[n:n+3] for n in range(0, len(landscape_images), 3)] + \
               [portrait_images[n:n+3] for n in range(0, len(portrait_images), 3)]



