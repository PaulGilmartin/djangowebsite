from django.db import models

# Maybe we should use ImageField in the below - bad practice to store files in static dir?


class AbstractMedia(models.Model):

    title = models.TextField()
    content_path = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.title[:100]

    def __repr__(self):
        return '<{} {}>'.format(self.__class__.__name__, self.title)


class Paper(AbstractMedia):

    abstract = models.TextField()
    coauthors = models.TextField(blank=True, default="")


class Project(AbstractMedia):

    abstract = models.TextField()
    coauthors = models.TextField(blank=True, default="")


class Picture(AbstractMedia):

    subject = models.TextField(null=True, blank=True, default='')
    location = models.TextField(null=True, blank=True, default='')
    orientation = models.TextField(default='Landscape')

    def modal_images(self):
        return [self.content_path]


