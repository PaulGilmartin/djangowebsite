from django.contrib import admin
from .models import Paper, Project, Picture

# Register your models here.

admin.site.register(Paper)
admin.site.register(Project)
admin.site.register(Picture)
