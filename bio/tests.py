from django.test import TestCase
from django.test import SimpleTestCase, TestCase
from django.urls import reverse
from .models import Paper, Picture
# Create your tests here.


class HomePageViewTests(SimpleTestCase):

    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_page_url_pattern(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_teemplate(self):
        response = self.client.get(reverse('home'))
        self.assertTemplateUsed(response, 'home.html')


class MediaModelTests(TestCase):

    def test_create_paper(self):
        Paper.objects.create(
            title='My great paper',
            abstract='This is amazing',
            content_path='wwww.mygreatpaper.com'
        )
        paper = Paper.objects.all()[0]
        self.assertEqual(paper.id, 1)
        self.assertEqual(paper.title, 'My great paper')
        self.assertEqual(paper.abstract, 'This is amazing')


class PublicationsPageViewTests(TestCase):

    def setUp(self):
        self.paper = Paper.objects.create(
                title='My great paper',
                abstract='This is amazing',
                content_path='wwww.mygreatpaper.com'
            )

    def test_publications_page_content(self):
        response = self.client.get(reverse('publications'))
        self.assertTemplateUsed(response, 'publications.html')
        self.assertContains(response, self.paper.abstract)


class GalleryPageViewTests(TestCase):

    def setUp(self):
        self.glasgow = Picture.objects.create(
            title='my pic',
            content_path='static/gallery/pic.jpg',
            location='Glasgow'
        )

    def test_glasgow_page_content(self):
        response = self.client.get('gallery/Glasgow')
        self.assertEqual(response.status_code, 200)

